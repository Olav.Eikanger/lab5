package labyrinth;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

import datastructure.CellPosition;
import datastructure.Grid;
import datastructure.GridDirection;
import datastructure.IGrid;

public class AStar {
    private IGrid<LabyrinthTile> grid;
    private IGrid<Scores> scores;

    
    AStar(IGrid<LabyrinthTile> grid, IGrid<Scores> scores) {
        this.grid = grid;
        this.scores = scores;
    }

    AStar(IGrid<LabyrinthTile> grid) {
        this(
            grid,
            new Grid<Scores>(grid.rows(), grid.cols(), new Scores(Integer.MAX_VALUE, Integer.MAX_VALUE))
        );
    }

    private record Scores(Integer f_score, Integer h_score) implements Comparable<Scores> {
        @Override
        public int compareTo(Scores other) {
            if (!this.f_score.equals(other.f_score)) {
                return (int) Math.signum(this.f_score() - other.f_score());
            }

            return (int) Math.signum(this.h_score() - other.h_score());
        }
    };

    private record QueueRecord(Scores scores, CellPosition pos) {}
    
    public CellPosition findPath(CellPosition start, CellPosition end) {
        PriorityQueue<QueueRecord> open = new PriorityQueue<>(10, new Comparator<QueueRecord>() {
            @Override
            public int compare(QueueRecord r1, QueueRecord r2) {
                return r1.scores.compareTo(r2.scores);
            }
        });
        open.add(new QueueRecord(new Scores(0, hScore(start, end)), start));
        scores.set(start, new Scores(0, hScore(start, end)));
        HashMap<CellPosition, CellPosition> aPath = new HashMap<>();
        CellPosition pos;
        do {
            if (open.isEmpty()) {
                return start;
            }

            pos = open.poll().pos();
            for (GridDirection direction : GridDirection.FOUR_DIRECTIONS) {
                CellPosition neighborPos = pos.getNeighbor(direction);
                if (!grid.isOnGrid(neighborPos)) {
                    continue;
                }
                LabyrinthTile tile = grid.get(neighborPos);
                if (tile.equals(LabyrinthTile.WALL) || tile.equals(LabyrinthTile.GOLD)) {
                    continue;
                }
                Scores tempScore = new Scores(fScore(pos), hScore(neighborPos, end));
                if (tempScore.compareTo(scores.get(neighborPos)) < 0) {
                    scores.set(neighborPos, tempScore);
                    open.add(createQueueRecord(neighborPos, pos, end));
                    aPath.put(neighborPos, pos);
                }
            }
        } while (!grid.get(pos).equals(LabyrinthTile.PLAYER));

        pos = end;
        ArrayList<CellPosition> finalPath = new ArrayList<>();
        while (!pos.equals(start)) {
            finalPath.add(pos);
            pos = aPath.get(pos);
        }

        if (finalPath.size() < 0) {
            return start;
        }
        
        return finalPath.get(finalPath.size() - 1);
    }

    private QueueRecord createQueueRecord(CellPosition pos, CellPosition prevPos, CellPosition end) {
        return new QueueRecord(
            new Scores(
                fScore(prevPos),
                hScore(pos, end)
            ),
            pos
        );
    }

    private Integer fScore(CellPosition prevPos) {
        return scores.get(prevPos).f_score() + 1;
    }

    private Integer hScore(CellPosition pos, CellPosition end) {
        return Math.abs(end.col() - pos.col()) + Math.abs(end.row() - pos.row());
    }
}
