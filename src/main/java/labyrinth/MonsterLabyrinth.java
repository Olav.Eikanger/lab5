package labyrinth;

import datastructure.CellPosition;
import datastructure.GridDirection;
import datastructure.IGrid;

public class MonsterLabyrinth extends Labyrinth {

    private CellPosition monsterPos;

    private boolean monsterSet;

    public MonsterLabyrinth(IGrid<LabyrinthTile> tiles) throws LabyrinthParseException {
        super(tiles);
        int numMonsters = 0;
		for (CellPosition pos : tiles.cellPositions()) {
			if (tiles.get(pos) == LabyrinthTile.MONSTER) {
				numMonsters++;
				monsterPos = pos;
				monsterSet = true;
			}
		}
		if (numMonsters != 1) {
			throw new LabyrinthParseException("Labyrinth created with " + numMonsters + " number of monsters!");
		}

        checkState(this);
    }

    public static void checkState(MonsterLabyrinth labyrinth) {
        boolean ok = !labyrinth.playerSet
                     || !labyrinth.monsterSet
                     || labyrinth.isValidPos(labyrinth.getPlayerPos())
                     || labyrinth.isValidPos(labyrinth.getMonsterPos());
		int numPlayers = 0;
		int numMonsters = 0;
		for (CellPosition pos : labyrinth.getTiles().cellPositions()) {
			if (labyrinth.getTiles().get(pos) == LabyrinthTile.PLAYER) {
				numPlayers++;
			} else if (labyrinth.getTiles().get(pos) == LabyrinthTile.MONSTER) {
				numMonsters++;
			}
		}
		if (labyrinth.playerSet) {
			ok &= numPlayers == 1;
		} else {
			ok &= numPlayers == 0;
		}
        if (labyrinth.monsterSet) {
			ok &= numMonsters == 1;
		} else {
			ok &= numMonsters == 0;
		}
		if (!ok) {
			throw new IllegalStateException("bad object");
		}
    }
    
    public void moveMonster() {
        CellPosition playerPos = this.getPlayerPos();
        CellPosition nextCell = new AStar(getTiles()).findPath(monsterPos, playerPos);
        this.getTiles().set(monsterPos, LabyrinthTile.OPEN);
        this.monsterPos = nextCell;
        this.getTiles().set(monsterPos, LabyrinthTile.MONSTER);
    }

    @Override
	public void movePlayer(GridDirection d) throws MovePlayerException {
        super.movePlayer(d);
        moveMonster();
    }

    protected CellPosition getMonsterPos() {
        return this.monsterPos;
    }
}
