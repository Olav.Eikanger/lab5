package labyrinth;

import labyrinth.gui.LabyrinthGUI;

public class Main {
	// Labyrinth
	public static char[][] testLabyrinth = { //
			{ '*', '*', '*', '*' }, //
			{ '*', ' ', ' ', ' ' }, //
			{ '*', ' ', '*', ' ' }, //
			{ ' ', ' ', 's', ' ' }, //
			{ '*', '*', ' ', '*' },
			{ 'm', '*', ' ', ' ' }, //
			{ ' ', '*', ' ', ' ' }, //
			{ '*', '*', ' ', '*' }, //
			{ ' ', ' ', ' ', '*' }, //
			{ ' ', ' ', ' ', '*' },  };

	public static void main(String[] args) {
		//LabyrinthGUI.run(() -> new MonsterLabyrinth(LabyrinthHelper.loadGrid(testLabyrinth)));
		LabyrinthGUI.run(() -> new MonsterLabyrinth(LabyrinthHelper.makeRandomGrid(20, 30)));
	}
}
