package datastructure;

public interface IGrid<T> extends GridDimension {
    void set(CellPosition pos, T element);

    T get(CellPosition pos);

    IGrid<T> copy();

}
