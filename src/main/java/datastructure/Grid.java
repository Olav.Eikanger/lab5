package datastructure;

import java.util.ArrayList;
import java.util.List;

public class Grid<T> implements IGrid<T> {
    private final List<T> cells;
	private final int columns;
	private final int rows;

    public Grid(int rows, int columns, T initElement) {
        if (rows <= 0 || columns <= 0) {
			throw new IllegalArgumentException();
		}

		this.columns = columns;
		this.rows = rows;
		this.cells = new ArrayList<>(columns * rows);
		for (int i = 0; i < columns * rows; ++i) {
			this.cells.add(initElement);
		}
    }

    public void checkCoordinate(CellPosition pos) {
		if (!this.isOnGrid(pos)) {
			throw new IndexOutOfBoundsException("Row and column indices must be within bounds");
		}
	}

    private int coordinateToIndex(CellPosition pos) {
		return pos.row() + pos.col() * rows;
	}

    @Override
    public int rows() {
        return rows;
    }

    @Override
    public int cols() {
        return columns;
    }

    @Override
    public void set(CellPosition pos, T element) {
        this.checkCoordinate(pos);
		this.cells.set(coordinateToIndex(pos), element);
    }

    @Override
    public T get(CellPosition pos) {
        checkCoordinate(pos);
		return this.cells.get(coordinateToIndex(pos));
    }

    @Override
    public IGrid<T> copy() {
        Grid<T> newGrid = new Grid<>(rows(), cols(), null);
		for (CellPosition pos : this.cellPositions()) {
			newGrid.set(pos, this.get(pos));
		}
		return newGrid;
    }
}
